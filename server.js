const Butter = require("./util/butter");
const { v4: uuidv4 } = require('uuid');
const Users = require("./util/db"); 

const PORT = 8000;
const USERS = Users;

const server = new Butter();

// Cache to store responses
const cache = new Map();

server.beforeEach((req, res, next) => {
    if (req.method === "GET" || req.method === "DELETE") {
        return next();
    }

    if (req.headers["content-type"] === "application/json") {
        let body = "";
        req.on("data", (chunk) => {
            body += chunk.toString("utf-8");
        });

        req.on("end", () => {
            body = JSON.parse(body);
            req.body = body;
            return next();
        });
    } else {
        next()
    }
});

server.route("get", "/api/users", (req, res) => {
    console.log('cache triggered');
    if (cache.has(req.url)) {
        const cachedResponse = cache.get(req.url);
        return res.status(200).json(cachedResponse);
    }

    const response = USERS;
    cache.set(req.url, response);
    res.setHeader('Cache-Control', 'public, max-age=3600');
    res.status(200).json(response);
    console.log('Response headers after setting cache:', res.getHeaders());
});



server.route("post", "/api/users", (req, res) => {
    const id = uuidv4();
    const name = req.body.name;
    const email = req.body.email;
    const hobbies = req.body.hobbies || []; 

    const user = {
        id: id,
        name: name,
        email: email,
        hobbies: hobbies,
    }

    USERS.push(user);
    res.status(201).json({message: "User was created successfully"});

    // Clear cache on POST request
    cache.clear();
});

server.route("get", "/api/users/user", (req, res) => {
    const userId = req.headers.userid;
    const user = USERS.find((user) => user.id === userId);

    if (user) {
        res.status(200).json({user: user, link: `/api/users/${userId}/hobbies`});
    } else {
        res.status(404).json({ error: "User was not found." });
    }
});


server.route("delete", "/api/users/:userId", (req, res) => {
    const urlParts = req.url.split("/");
    const userId = urlParts[urlParts.length - 1];
    const userIndex = USERS.findIndex((user) => user.id === userId);

    if (userIndex > -1) {
        USERS.splice(userIndex, 1);
        res.status(200).json({ message: "User was deleted successfully." });
        // Clear cache on DELETE request
        cache.clear();
    } else {
        res.status(404).json({ error: "User was not found." });
    }
});

server.route("get", "/api/users/:userId/hobbies", (req, res) => {
    const urlParts = req.url.split("/");
    const userId = urlParts[urlParts.length - 2];
    const userIndex = USERS.findIndex((user) => user.id === userId);

    if (userIndex > -1) {
        const userHobbies = USERS[userIndex].hobbies;
        const hobbiesWithLinks = userHobbies.map(hobby => ({
            hobbies: hobby,
            user: `/api/users/${userId}`
        }));
        res.status(200).json(hobbiesWithLinks);
    } else {
        res.status(404).json({ error: "User was not found." });
    }
});

server.route("patch", "/api/users/:userId/hobbies", (req, res) => {
    const updatedHobbies = req.body.hobbies; 
    const urlParts = req.url.split("/");
    const userId = urlParts[urlParts.length - 2];

    const userIndex = USERS.findIndex((user) => user.id === userId);

    if (userIndex > -1) {
        let userHobbies = USERS[userIndex].hobbies;

        updatedHobbies.forEach((updatedHobby) => {
            if (!userHobbies.includes(updatedHobby)) {
                userHobbies.push(updatedHobby);
            }
        });

        USERS[userIndex].hobbies = userHobbies;

        res.status(200).json(userHobbies);
        cache.clear();
    } else {
        res.status(404).json({ error: "User was not found." });
    }
});


server.listen(PORT, () => {
    console.log(`Server is running on port: ${PORT}`)
});
