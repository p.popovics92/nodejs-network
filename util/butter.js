const http = require("http");
const fs = require('node:fs/promises')

class Butter {
    
    constructor() {
        this.server = http.createServer();
        this.routes = {};
        this.middleware = [];

        this.server.on("request", (req, res) => {
            res.sendFile = async (path, mime) => {
                const fileHandler = await fs.open(path, "r");
                const fileStream = fileHandler.createReadStream();

                res.setHeader("Content-Type", mime);

                fileStream.pipe(res);
            };

            res.status = (code) => {
                res.statusCode = code;
                return res;
            }

            res.json = (message) => {
                res.setHeader("Content-Type", "application/json");
                res.end(JSON.stringify(message)); 
            }

            const runMiddleware = (req, res, middleware, index) => {
                if (index === middleware.length) {
                    if (!this.routes[req.method.toLowerCase() + req.url]) {
                        if (req.method === "DELETE" && req.url.startsWith("/api/users/")) {
                            const deleteRouteHandler = this.routes["delete/api/users/:userId"];
                            if (deleteRouteHandler) {
                                deleteRouteHandler(req, res); 
                                return; 
                            }
                        }

                        if ((req.method === "GET" ) && req.url.endsWith("/hobbies")) {
                            const hobbiesRouteHandler = this.routes["get/api/users/:userId/hobbies"];
                            if (hobbiesRouteHandler) {
                                hobbiesRouteHandler(req, res); 
                                return; 
                            }
                        }
                        
                        if (req.method === "PATCH" && req.url.endsWith("/hobbies")) {
                            const hobbiesRouteHandler = this.routes["patch/api/users/:userId/hobbies"];
                            if (hobbiesRouteHandler) {
                                hobbiesRouteHandler(req, res); 
                                return; 
                            }
                        }

                        return res.status(404).json({ error: `Cannot ${req.method} ${req.url}` });
                    }
            
                    this.routes[req.method.toLowerCase() + req.url](req, res);
                } else {
                    middleware[index](req, res, () => {
                        runMiddleware(req, res, middleware, index + 1);
                    });
                }
            };
            
            runMiddleware(req, res, this.middleware, 0);
        });
    }

    route(methode, path, cb) {
        this.routes[methode + path] = cb;
        
    };

    beforeEach(cb) {
        this.middleware.push(cb);
    }

    listen(port, callback) {
        this.server.listen(port, () => {
            callback();
        });
    };
}

module.exports = Butter;