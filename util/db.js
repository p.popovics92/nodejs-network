const Users = [
    {
        id: "5f3b4b29-03dd-4ed9-84a3-6dfcfz4c2be98",
        name: "Test",
        email: "test@email.com",
        hobbies: [
            'dancing'
        ]
    },
    {
        id: "5f3b4b29-03dd-4ed9-84a3-6dfcfz4c2xxxx",
        name: "Test2",
        email: "test2@email.com",
        hobbies: [
            'dancing',
            'sport'
        ]
    },
    {
        id: "5f3b4b29-03dd-4ed9-84a3-6dfcfz4c2yyyy",
        name: "Test3",
        email: "test3@email.com",
        hobbies: []
    }
];

module.exports = Users;